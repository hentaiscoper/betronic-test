import React from 'react';
import Input from '../../shared/input';
import block from 'bem-cn';
import './styles.scss';
import Radio from '../../shared/radio';
import Select from '../../shared/select';
import { datepickerModel, modalModel, formModel } from '../../entities';
import { useStore } from 'effector-react';

const b = block('create-account');

const CreateAccount = () => {
  const firstName = useStore(formModel.firstName$);
  const lastName = useStore(formModel.lastName$);
  const email = useStore(formModel.email$);
  const password = useStore(formModel.password$);
  const repeatPassword = useStore(formModel.repeatPassword$);

  const day = useStore(formModel.day$);
  const month = useStore(formModel.month$);
  const year = useStore(formModel.year$);

  const gender = useStore(formModel.gender$);

  const daysOptions = React.useMemo(
    () => datepickerModel.getDaysInMonth(Number(month), Number(year)),
    [month, year]
  );
  const yearsOptions = datepickerModel.getYears();

  React.useEffect(() => {
    return () => formModel.resetForm();
  }, []);

  return (
    <div className={b()}>
      <div className={b('header')}>
        <span className={b('title')}>Создать аккаунт</span>
        <span>Быстро и лекго.</span>
      </div>

      <div className={b('body')}>
        <div className={b('row')}>
          <Input
            placeholder="Имя"
            value={firstName}
            onChange={formModel.setFirstName}
          />
          <Input
            placeholder="Фамилия"
            value={lastName}
            onChange={formModel.setLastName}
          />
        </div>
        <Input
          placeholder="Номер мобильного телефона или эл. адрес"
          value={email}
          onChange={formModel.setEmail}
        />
        <Input
          type="password"
          placeholder="Новый пароль"
          value={password}
          onChange={formModel.setPassword}
        />
        <Input
          type="password"
          placeholder="Повторите пароль"
          value={repeatPassword}
          onChange={formModel.setRepeatPassword}
        />
        <div className={b('row-title')}>Дата рождения</div>
        <div className={b('row')}>
          <Select
            value={day}
            options={daysOptions}
            onChange={formModel.setDay}
          />
          <Select
            value={month}
            options={datepickerModel.monthsOptions}
            onChange={formModel.setMonth}
          />
          <Select
            value={year}
            options={yearsOptions}
            onChange={formModel.setYear}
          />
        </div>
        <div className={b('row-title')}>Пол</div>
        <div className={b('row')}>
          <Radio
            id="male"
            value={gender}
            onChange={formModel.setGender}
            label="Мужской"
          />
          <Radio
            id="female"
            value={gender}
            onChange={formModel.setGender}
            label="Женский"
          />
          <Radio
            id="another"
            value={gender}
            onChange={formModel.setGender}
            label="Другой"
          />
        </div>
        <p className={b('description')}>
          Люди, которые пользуются нашим сервисом, могли загрузить вашу
          контактную информацию на Facebook. <a>Подробнее</a>
        </p>
        <p className={b('description')}>
          Нажимая кнопку Регистрация, вы принимаете{' '}
          <a>
            Условия, Политику использованияя данных и Политику в отношении
            файлов cookie
          </a>
          . Вы можете получать от нас SMS-уведомления, отказаться от которых
          можно в любой момент.
        </p>
        <button
          className={b('submit')}
          onClick={() => modalModel.openModal(<div>You have registered!</div>)}>
          Регистрация
        </button>
        <a className={b('have-account')}>У вас уже есть аккаунт?</a>
      </div>
    </div>
  );
};

export default CreateAccount;
