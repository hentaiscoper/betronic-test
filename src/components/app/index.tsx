import React from 'react';
import Modal from '../modal';
import { modalModel } from '../../entities';
import CreateAccount from '../../features/create-account';
import './styles.scss';

function App() {
  return (
    <div className="container">
      <Modal />
      <button onClick={() => modalModel.openModal(<CreateAccount />)}>
        Open
      </button>
    </div>
  );
}

export default App;
