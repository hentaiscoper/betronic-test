import React from 'react';
import { useStore } from 'effector-react';
import block from 'bem-cn';

import { modalModel } from '../../entities';
import './styles.scss';

const b = block('modal');

const Modal = () => {
  const isOpen = useStore(modalModel.isOpen$);
  const modalContent = useStore(modalModel.content$);

  return (
    <div className={b({ isOpen })}>
      {isOpen ? (
        <>
          <div
            className={b('overlay')}
            onClick={() => modalModel.closeModal()}
          />
          <div className={b('container')}>{modalContent}</div>
        </>
      ) : null}
    </div>
  );
};

export default Modal;
