import block from 'bem-cn';
import React from 'react';
import './styles.scss';

type Props = {
  label?: string;
  value: string | null;
  id: string;
  onChange?: (value: string) => void;
};

const b = block('option');

const Radio = ({ label, value, id, onChange }: Props) => {
  const onRadioChange = () => {
    if (onChange) onChange(id);
  };
  return (
    <label className={b({ active: id === value })}>
      <span className={b('label')}>{label}</span>
      <input
        className={b('control')}
        checked={id === value}
        type="radio"
        onChange={onRadioChange}
      />
    </label>
  );
};

export default Radio;
