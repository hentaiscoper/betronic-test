import React from 'react';
import block from 'bem-cn';
import './styles.scss';

const b = block('select');

type OptionItem = {
  value: number;
  label: number | string;
};

type Props = {
  value: string;
  options?: OptionItem[];
  onChange?: (value: string) => void;
};

const Select = ({ value, options, onChange }: Props) => {
  const optionsObj = React.useMemo(() => (options ? options : []), [options]);

  const selectOnChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    if (onChange) onChange(selectedValue);
  };

  return (
    <select className={b()} value={value} onChange={selectOnChange}>
      {optionsObj.map((option) => (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      ))}
    </select>
  );
};

export default Select;
