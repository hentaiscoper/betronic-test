import block from 'bem-cn';
import React, { ChangeEvent } from 'react';
import './styles.scss';

const b = block('input');

type Props = {
  placeholder?: string;
  type?: string;
  value?: string;
  onChange?: (value: string) => void;
};

const Input = ({ placeholder, type, value, onChange }: Props) => {
  const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (onChange) onChange(e.target.value);
  };

  return (
    <label className={b()}>
      <input
        type={type}
        placeholder={placeholder}
        className={b('control')}
        value={value}
        onChange={onInputChange}
      />
    </label>
  );
};

export default Input;
