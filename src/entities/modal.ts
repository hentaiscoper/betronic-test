import { rootDomain } from './root-domain';
import { ReactNode } from 'react';
import { forward } from 'effector';
import { delay } from 'patronum';

export const domain = rootDomain.createDomain('modal');
export const reset = domain.event('reset');

export const isOpen$ = domain.store<boolean>(false);
export const content$ = domain.store<ReactNode | null>(null);

export const openModal = domain.event<ReactNode>('openModal');
export const setData = domain.event<ReactNode>('setData');
export const closeModal = domain.event('closeModal');

const delayedSetData = delay({
  source: openModal,
  timeout: 1,
});

forward({
  from: openModal,
  to: closeModal,
});

forward({
  from: delayedSetData,
  to: setData,
});

openModal.watch(() => console.log('open'));
setData.watch(() => console.log('set'));
closeModal.watch(() => console.log('close'));

isOpen$.on(setData, () => true).reset(closeModal);
content$.on(setData, (value, payload) => payload).reset(closeModal);
