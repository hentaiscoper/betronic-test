import {createDomain} from "effector";

export const rootDomain = createDomain('root');
export const rootReset = rootDomain.event('reset');

