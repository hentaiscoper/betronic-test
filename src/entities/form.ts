import { rootDomain } from './root-domain';
import type { Store, Event } from 'effector';

export const resetForm = rootDomain.event('resetForm');

export const createInput = ({
  initialState = '',
  name,
}: {
  initialState?: string;
  name: string;
}): [Store<string>, Event<string>] => {
  const inputDomain = rootDomain.domain(name);

  const store$ = inputDomain.store(initialState);
  const setStore = inputDomain.event<string>('setStore');

  store$.on(setStore, (_, value) => value).reset(resetForm);

  return [store$, setStore];
};

export const [email$, setEmail] = createInput({ name: 'email' });
export const [password$, setPassword] = createInput({ name: 'password' });
export const [repeatPassword$, setRepeatPassword] = createInput({
  name: 'repeatPassword',
});
export const [firstName$, setFirstName] = createInput({ name: 'firstName' });
export const [lastName$, setLastName] = createInput({ name: 'lastName' });

export const [day$, setDay] = createInput({ name: 'day' });
export const [month$, setMonth] = createInput({ name: 'month' });
export const [year$, setYear] = createInput({ name: 'year' });

export const [gender$, setGender] = createInput({ name: 'gender' });
