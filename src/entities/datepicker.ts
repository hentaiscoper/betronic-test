export const getDaysInMonth = (month: number, year: number) => {
  const days = new Date(year, month, 0).getDate();
  const result = [];

  for (let i = 0; i <= days; i++) {
    result.push(i);
  }

  return result.map((i) => ({ value: i, label: i === 0 ? 'День' : `${i}` }));
};

export const monthsOptions = [
  'Месяц',
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь',
].map((label, value) => ({ value, label }));

export const getYears = () => {
  const max = new Date().getFullYear();
  const min = max - 100;
  let result = [0];

  for (let i = max; i >= min; i--) {
    result.push(i);
  }

  return result.map((i) => ({ value: i, label: i === 0 ? 'Год' : `${i}` }));
};
